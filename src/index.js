import "./js/accordion";
import Scroller from "./js/Scroller";
import blocks from "./js/blocks";
import Gallery from "./js/Gallery";
import intro from "./js/intro";
import IMask from 'imask';

import images from "./assets/gallery/images.json";

function modal(modalName, display = true) {
    const contacts = document.getElementById('modal-contacts');
    const done = document.getElementById('modal-done');
    contacts.style.display = modalName === 'contacts' && display ? 'block' : 'none';
    done.style.display = modalName === 'done' && display ? 'block' : 'none';
    window.history.replaceState(null, null,
        modalName === 'done' && display ? '?will-be-warmer=soon' : '/'
    );
}

function buildModals() {
    document.getElementById('steps2').addEventListener('click', () => {
        modal('contacts');
    });
    for (const button of document.getElementsByClassName('close'))
        button.addEventListener('click', () => {
            modal(null);
        });

    for (const modalBg of document.getElementsByClassName('modal'))
        modalBg.addEventListener('click', (e) => {
            if (e.target === modalBg)
                modal(null);
        });
}

function applyMasks() {
    for (const input of document.getElementsByClassName('phone-input'))
        new IMask(
            input,
            {mask: '+{38} (000) 000-00-00'}
        );
}

function sendForm(formData) {
    return new Promise((res, rej) => {
        console.log(formData);

        const http = new XMLHttpRequest();
        http.open('POST', '/contacts.php', true);
        http.setRequestHeader('Content-type', 'application/json; charset=UTF-8');

        http.onreadystatechange = function () {
            if (http.readyState === 4) {
                if (http.status === 200) res();
                else rej();
            }
        };
        http.send(JSON.stringify(formData));
    });
}


function buildForms() {

    function handler(data) {
        const info = {
            name: data.get('name'),
            phone: data.get('phone')
        };
        if (info.phone.length < 19) {
            alert('Проверьте правильность введенного телефона и попробуйте заново');
            return false;
        }
        sendForm(info).then(() => {
            // modal('done');
            window.location.href = 'https://dpcomfort.com.ua/?will-be-warmer=soon';
        }).catch(() => {
            alert('Что-то пошло не так! Попробуйте связаться с нами по телефону');
        });
        return false;
    }

    for (const form of document.getElementsByTagName('form')) {
        form.onsubmit = (e) => {
            e.preventDefault();
            return handler(new FormData(form));
        };
    }

}

function scrollTo(elem, style, unit, from, to, time, prop) {
    if (!elem) {
        return;
    }
    var start = new Date().getTime(),
        timer = setInterval(function () {
            var step = Math.min(1, (new Date().getTime() - start) / time);
            if (prop) {
                elem[style] = (from + step * (to - from))+unit;
            } else {
                elem.style[style] = (from + step * (to - from))+unit;
            }
            if (step === 1) {
                clearInterval(timer);
            }
        }, 25);
    if (prop) {
        elem[style] = from+unit;
    } else {
        elem.style[style] = from+unit;
    }
}

function getQueryVariable(variable) {
    let query = window.location.search.substring(1);
    let vars = query.split("&");
    for (let i=0;i<vars.length;i++) {
        let pair = vars[i].split("=");
        if(pair[0] === variable){return pair[1].toLowerCase();}
    }
    return(false);
}

window.onload = () => {

    // document.documentElement.scrollTop = 0;


    intro(() => {

        const scroller = new Scroller(blocks, id => {
            if (id === 'top') return document.getElementById('content').classList.add('hidden');
            else document.getElementById('content').classList.remove('hidden');
            for (const el of document.getElementsByClassName('content-item')) {
                if (el.id === `content-${id}`) el.classList.add('selected');
                else el.classList.remove('selected');
            }
        });
        // const el = document.getElementsByClassName('intro')[0];
        // el.remove();
        applyMasks();
        buildForms();
        buildModals();

        // modal('done');
        // window.open('http://0.0.0.0:8080/?will-be-warmer=done');

        const targetSection = getQueryVariable("show");
        const scrollToSection = getQueryVariable("scrollTo");
        const openedModal = getQueryVariable("will-be-warmer");

        if (openedModal && openedModal === 'soon') {
            modal('done');
        }

        if (scrollToSection && document.getElementById(scrollToSection)) {
            const target = document.getElementById(scrollToSection);
            scrollTo(document.scrollingElement || document.documentElement, "scrollTop", "", 0, target.offsetTop, 2000, true);
        }

        if (targetSection) {
            document.getElementById(targetSection).scrollIntoView();
        }
    });


    new Gallery("gallery", images, {
        interval: 5000,
        intervalPause: 10000
    }).init();

};

