
import { animate } from './utils';

const ANIMATION = 'fadeInUp';

const animateEnter = (id, delay) => {
	if (delay)
		setTimeout(() => animateEnter(id), delay);
	else
		document.getElementById(id).classList.add('shown');
};

/**
 * @param {Number} n
 * @return {Block}
 */
const contact = n => ({
	element: document.getElementById(`contact${n}1`),
	// onShowOnce() {
	// 	animate([`contact${n}1`, `contact${n}2`, `contact${n}3`], ANIMATION);
	// }
});

const blocks = [

	{
		element: document.getElementById('top'),
		// onShowOnce() {
		// 	animate(['top1', 'top2', 'top3', 'top4'], ANIMATION);
		// }
	},

	{
		element: document.getElementById('facts'),
		// onShowOnce() {
		// 	setTimeout(() => {
		// 		animate('facts', ANIMATION, () => {
		// 			animate(['fact2', 'fact3', 'fact4', 'fact5'], ANIMATION);
		// 		});
		// 	}, 1000);
        //
		// },
	},

	{
		element: document.getElementById('why'),
		// onShowOnce() {
		// 	animate(['why1', 'why2', 'why3', 'why4', 'why5'], ANIMATION);
		// }
	},

	{
		element: document.getElementById('photos'),
		// onShowOnce() {
		// 	animate('photos1', ANIMATION);
		// }
	},

    contact(1),

    {
		element: document.getElementById('steps'),
		onShowOnce() {
			animate(['steps1'], ANIMATION, el => {
				this.element.getElementsByClassName('card')[0].classList.add('selected');
				this.element.getElementsByClassName('card-top')[0].classList.add('shown');
			});
		}
    },

    {
		element: document.getElementById('packets'),
		_selected() {
			this.element.getElementsByClassName('selected')[0].classList.add('selected-shown');

			const container = this.element.getElementsByClassName('table-container')[0];
			const a = container.offsetWidth, b = this.element.getElementsByClassName('table-body')[0].offsetWidth;
			if (b > a) container.scrollLeft = (b - a) / 2;
		},
		onShowOnce() {
			// animate(['packets1', 'packets2', 'packets3', 'packets4', 'packets5'], ANIMATION, el => {
			// 	if (el.id === 'packets5')
			// 		this._selected();
			// });
		},
		onShow() {
			if (this.onShowOnce) return;
			this._selected();
		},
		onHide() {
			this.element.getElementsByClassName('selected')[0].classList.remove('selected-shown');
		}
    },
    {
		element: document.getElementById('reviews'),
		// onShowOnce() {
		// 	animate('reviews1', ANIMATION, () => {
		// 		animate(Array.from(this.element.getElementsByClassName('comment')), ANIMATION);
		// 	});
		// }
    },

    contact(2),

    {
		element: document.getElementById('confidence'),
		// onShowOnce() {
		// 	animate(['confidence1', 'confidence2', 'confidence3', 'confidence4', 'confidence5'], ANIMATION);
		// }
    },

    {
		element: document.getElementById('materials')
    },

    {
    	element: document.getElementById('faq')
    },

    contact(3)

];

export default blocks;
