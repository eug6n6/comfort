
// export const onAnimationEnd

export const animate = (el, animation, cb) => {
	if (Array.isArray(el))
		return el.map(_ => animate(_, animation, cb));

	if (typeof el === 'string')
		el = document.getElementById(el);

	el.classList.remove('v-hidden');
	el.classList.add('animated', animation);

	const animationEndEventName = [
		['animation', 'animationend'],
		['OAnimation', 'oAnimationEnd'],
		['MozAnimation', 'mozAnimationEnd'],
		['WebkitAnimation', 'webkitAnimationEnd']
	].find(_ => el.style[_[0]] !== undefined)[1];

	const onEnd = () => {
		if (cb) cb(el);
		el.classList.remove(animation);
		el.removeEventListener(animationEndEventName, onEnd);
	};
	el.addEventListener(animationEndEventName, onEnd);
};