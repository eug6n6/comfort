
// const acc = document.getElementsByClassName("accordion");
//
// for (let i = 0; i < acc.length; i++) {
// 	acc[i].addEventListener("click", function() {
// 		this.classList.toggle("active");
// 		const panel = this.nextElementSibling;
// 		panel.classList.toggle("collapsed");
// 		// if (panel.style.maxHeight) {
// 		// 	panel.style.maxHeight = null;
// 		// } else {
// 		// 	panel.style.maxHeight = "fit-content";
// 		// }
// 		// if (panel.style.flex === '0') {
// 		// 	panel.style.flex = '1';
// 		// } else {
// 		// 	panel.style.flex = "0";
// 		// }
// 	});
// }

const init = () => {
	const acc = document.getElementsByClassName("accordion");

	for (let i = 0; i < acc.length; i++) {
		acc[i].addEventListener("click", function() {
			this.classList.toggle("active");
			const panel = this.nextElementSibling;
			if (panel.style.maxHeight){
				panel.style.maxHeight = null;
			} else {
				panel.style.maxHeight = panel.scrollHeight + "px";
			}
		});
	}
};

init();
