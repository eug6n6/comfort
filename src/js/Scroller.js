/**
 * @typedef {Object} Block
 * @property {HTMLElement} element
 * @property {Function} [onShow]
 * @property {Function} [onShowOnce]
 * @property {Function} [onHide]
 * @property {Function} [onHideOnce]
 * @property {Function} [onScroll]
 */

export default class Scroller {
	/**
	 * @param {Block[]} blocks
	 * @param {function(id: String)} onCurrentBlockChangedFunc
	 */
	constructor(blocks, onCurrentBlockChangedFunc) {
		this.blocks = blocks;
		/**
		 * @type {function(blockId: String)}
		 * @private
		 */
		this.onCurrentBlockChangedFunc = onCurrentBlockChangedFunc;
		/**
		 * @type {Block}
		 */
		this.currentBlock = undefined;

		// this.currentBlocks = [];

		this.doc = document.body || document.documentElement;

		document.body.onscroll = () => {
			this.onScroll();
		};

		setTimeout(() => this.onScroll(), 100);
		this.onScroll();
	}

	/**
	 * @private
	 */
	onScroll() {
		// const newCurrentBlocks = this.blocks.filter(block => {
		// 	const area = Scroller.getBlockArea(block);
		// 	return this.doc.scrollTop > area.top && area.bottom > (this.doc.scrollTop + window.innerHeight)
		// 		|| this.doc.scrollTop < area.top && area.top < (this.doc.scrollTop + window.innerHeight)
		// 		|| this.doc.scrollTop < area.bottom && area.bottom < (this.doc.scrollTop + window.innerHeight);
		// });
		//
		// const set = new Set(this.currentBlocks.concat(newCurrentBlocks));
		// for (const block of set) {
		// 	if (this.currentBlocks.includes(block) && !newCurrentBlocks.includes(block)) {
		// 		Scroller.hideBlock(block);
		// 	} else if (!this.currentBlocks.includes(block) && newCurrentBlocks.includes(block)) {
		// 		Scroller.showBlock(block);
		// 	}
		// }
		// this.currentBlocks = newCurrentBlocks;

		const visibleBlocks = this.blocks.filter(block => {
			const area = Scroller.getBlockArea(block);
			const a = Scroller.scrollTop, b = Scroller.scrollTop + Scroller.scrollHeight;
			return a > area.top && area.bottom > b
				|| a < area.top && area.top < b
				|| a < area.bottom && area.bottom < b;
		});

		const newCurrentBlock = visibleBlocks.sort((b1, b2) => {
			const windowMidPoint = this.doc.scrollTop + window.innerHeight / 2;
			const blockMidPoint1 = b1.element.offsetTop + b1.element.offsetHeight / 2;
			const blockMidPoint2 = b2.element.offsetTop + b1.element.offsetHeight / 2;
			if (Math.abs(windowMidPoint - blockMidPoint1) < Math.abs(windowMidPoint - blockMidPoint2))
				return -1;
			else return 1;

			// if (b1.element.offsetTop > b2.element.offsetTop) {
			// 	if ((b1.element.offsetTop + b1.element.offsetHeight - Scroller.scrollTop) / b1.element.offsetHeight > (Scroller.scrollTo + window.innerHeight - b1.element.offsetTop) / b2.element.offsetHeight)
			// 		return -1;
			// 	else return 1;
			// } else {
			// 	if ((b1.element.offsetTop + b1.element.offsetHeight - Scroller.scrollTop) / b1.element.offsetHeight > (Scroller.scrollTo + window.innerHeight - b1.element.offsetTop) / b2.element.offsetHeight)
			// 		return 1;
			// 	else return -1;
			// }
		})[0];

		for (const block of visibleBlocks) {
			Scroller.showOnceBlock(block);
			Scroller.hideOnceBlock(block);
		}

		if (newCurrentBlock !== this.currentBlock) {
			if (this.currentBlock) Scroller.hideBlock(this.currentBlock);
			if (newCurrentBlock) Scroller.showBlock(newCurrentBlock);
			if (newCurrentBlock && typeof this.onCurrentBlockChangedFunc === 'function')
				this.onCurrentBlockChangedFunc(newCurrentBlock.element.id);
			this.currentBlock = newCurrentBlock;
		} else if (typeof this.currentBlock.onScroll === 'function') {
			const area = Scroller.getBlockArea(this.currentBlock);
			const windowMidPoint = Scroller.scrollTop + window.innerHeight / 2;
			const blockMidPoint = (area.bottom - area.top) / 2;
			const val = (windowMidPoint - blockMidPoint) / this.currentBlock.element.offsetHeight ;/// ((area.bottom - area.top) / 2);
			// console.log(val);
			this.currentBlock.onScroll(val);
		}

	}

	static get scrollTop() {
		return Math.max(document.documentElement.scrollTop, document.body.scrollTop);
	}
	static get scrollHeight() {
		return window.innerHeight;
	}

	// static getBlockMidpoint(block) {
	// 	return block.element.offsetTop + block.element.offsetHeight / 2;
	// }
	/**
	 * @param {Block} block
	 * @return {{top: Number, bottom: Number}}
	 * @private
	 */
	static getBlockArea(block) {
		return {
			top: block.element.offsetTop + block.element.offsetHeight / 3,
			bottom: block.element.offsetTop + block.element.offsetHeight * 2 / 3
		};
	}

	/**
	 * @param {Block} block
	 * @private
	 */
	static hideBlock(block) {
		if (typeof block.onHide === 'function')
			block.onHide();
	}

	/**
	 * @param {Block} block
	 * @private
	 */
	static showBlock(block) {
		if (typeof block.onShow === 'function')
			block.onShow();
	}

	/**
	 * @param {Block} block
	 * @private
	 */
	static hideOnceBlock(block) {
		if (typeof block.onHideOnce === 'function') {
			block.onHideOnce();
			block.onHideOnce = undefined;
		}
	}

	/**
	 * @param {Block} block
	 * @private
	 */
	static showOnceBlock(block) {
		if (typeof block.onShowOnce === 'function') {
			block.onShowOnce();
			block.onShowOnce = undefined;
		}
	}

}

