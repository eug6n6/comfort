
import { animate } from './utils';

const intro = (cb) => {
	const el = document.getElementsByClassName('intro')[0];

	setTimeout(() => {
		animate(document.getElementById('house'), 'fadeInRight');
		animate(document.getElementById('header'), 'fadeInUp');

		animate(el, 'flipOutX', () => {
			document.body.classList.remove('begin');
			document.getElementsByTagName('main')[0].style.display = 'block';
			el.remove();
			cb();
		});
	}, 3000);

	// document.getElementById('order-button').addEventListener('click', () => {
	// 	document.getElementById('contact-name').focus();
	// });

	document.getElementById('contact13').addEventListener('mouseover', () => {
		document.getElementById('contact1-cold').classList.add('not-cold');
	});
	document.getElementById('contact13').addEventListener('mouseleave', () => {
		document.getElementById('contact1-cold').classList.remove('not-cold');
	});
	document.getElementById('contact23').addEventListener('mouseover', () => {
		document.getElementById('contact2-cold').classList.add('not-cold');
	});
	document.getElementById('contact23').addEventListener('mouseleave', () => {
		document.getElementById('contact2-cold').classList.remove('not-cold');
	});

	// document.getElementById('steps').scrollIntoView();
};

export default intro;
