/**
 * @typedef {Object} ImageData
 * @property {String} url
 * @property {String} preview
 */

class Gallery {
	/**
 	 * @param {String} elementId
	 * @param {ImageData[]} images
	 * @param {{interval: Number, intervalPause: Number}} options
	 */
	constructor(elementId, images, options = {}) {
		this.images = images;
		this.current = 0;
		this.galleryElement = document.getElementById(elementId);

		this.interval = typeof options.interval === 'number' ? options.interval : 3000;
		this.intervalPause = options.intervalPause || 5000;

		this.currentImageElement = this.galleryElement.getElementsByClassName('current')[0];
		this.previewsElement = this.galleryElement.getElementsByClassName('previews')[0];
	}

	init() {
		const previewUrls = this.images.map(image => image.preview || image.url);
		for (let i = 0; i < previewUrls.length; i++) {
			const el = document.createElement("img");
			el.setAttribute('src', previewUrls[i]);
			el.setAttribute('class', 'preview');
			el.addEventListener('click', () => this.select(i));
			this.previewsElement.appendChild(el);
		}

		this.galleryElement.getElementsByClassName('count')[0].innerHTML = ' - ' + this.images.length;
		this.galleryElement
			.getElementsByClassName('to-left')[0]
			.addEventListener('click', () => this.select(this.prev));
		this.galleryElement
			.getElementsByClassName('to-right')[0]
			.addEventListener('click', () => this.select(this.next));

		this.select(this.current, false);
		this.detectSwipes(this.galleryElement.getElementsByClassName('body')[0]);
	}

	startInterval() {
		this.intervalInstance = setInterval(
			() => this.select(this.next, false),
			this.interval
		);
	}

	pauseInterval() {
		clearInterval(this.intervalInstance);
		if (this.pauseIntervalTimeout) clearTimeout(this.pauseIntervalTimeout);
		this.pauseIntervalTimeout = setTimeout(
			() => this.startInterval(),
			this.intervalPause
		);
	}

	/**
	 * @param {Node} element
	 * @private
	 */
	detectSwipes(element) {
		let xDown = null;
		let yDown = null;
		element.addEventListener('touchstart', event => {
			xDown = event.touches[0].clientX;
			yDown = event.touches[0].clientY;
		}, false);
		element.addEventListener('touchmove', event => {
			if ( ! xDown || ! yDown ) return;
			const xDiff = xDown - event.touches[0].clientX;
			const yDiff = yDown - event.touches[0].clientY;
			if ( Math.abs(xDiff) < Math.abs(yDiff) ) return;
			if ( xDiff > 0 ) 	this.select(this.next);
			else 				this.select(this.prev);
			xDown = null;
			yDown = null;
		}, false);
	}

	/**
	 * @param {Number} index
	 * @param {Boolean} [pauseInterval = true]
	 * @private
	 */
	select(index, pauseInterval = true) {
		const previews = this.galleryElement.getElementsByClassName('preview');
		previews[this.current].classList.remove('selected');
		this.current = index;
		previews[this.current].classList.add('selected');
		// const offset = Math.floor(document.documentElement.offsetWidth / 3) - previews[this.current].offsetLeft;
		const offset = Math.floor(document.documentElement.offsetWidth / 9) - previews[this.current].offsetLeft;
		// const offset = - previews[this.current].offsetLeft;
		// const offset = 0;
		this.galleryElement.getElementsByClassName('previews')[0].style.transform = `translateX(${offset}px)`;
		if (this.interval && pauseInterval) this.pauseInterval();

		this.nextImage.setAttribute('src', this.images[this.current].url);
		this.currentImageElement.classList.add('transit');

		this.galleryElement.getElementsByClassName('current-i')[0].innerHTML = index + 1;

		setTimeout(() => {
			this.currentImageElement.setAttribute('src', this.images[this.current].url);
			this.currentImageElement.classList.remove('transit');
		}, 1000);
	}

	/**
	 * @return {number}
	 * @private
	 */
	get count() {
		return this.images.length;
	}

	/**
	 * @return {Node}
	 * @private
	 */
	// get prevImage() {
	// 	return this.galleryElement.getElementsByClassName('prev')[0];
	// }

	/**
	 * @return {Node}
	 * @private
	 */
	get nextImage() {
		return this.galleryElement.getElementsByClassName('next')[0];
	}

	/**
	 * @return {number}
	 * @private
	 */
	get prev() {
		return this.current - 1 < 0 ? this.count - 1 : this.current - 1;
	}

	/**
	 * @return {number}
	 * @private
	 */
	get next() {
		return this.current + 1 >= this.count ? 0 : this.current + 1;
	}

}

export default Gallery;
