'use strict';
let webpack = require('webpack');
const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');



module.exports = {
	entry: ['./src/index.js', './src/style.scss'],

	/*Webpack 2 has a very strict configuration--
	  that was moved to the plugins section using the webpack.LoaderOptionsPlugin */
	plugins: [
		new HtmlWebpackPlugin({
			title: 'Днепрокомфорт',
			template: 'src/index.html',
			inject: 'body',
			minify:  true
		}),
		new CleanWebpackPlugin(['dist']),
		new webpack.optimize.UglifyJsPlugin({
			minimize: true
		})
	],

	output: {
		filename: 'index_bundle.js',
		//absolute path
		path: __dirname + '/dist',
		//global variable
	},

	/* a source map consists of a whole bunch of information that can
	 be used to map the code within a compressed file back to it’s original source */
	devtool: 'inline-source-map',

	watch: true,

	watchOptions: {
		aggregateTimeout: 100
	},

	// resolve: {
	// 	modules:[
	// 		path.resolve(__dirname), path.resolve(__dirname, "node_modules")
	// 	],
	// 	alias: {
	// 		"TweenLite": path.resolve('node_modules', 'gsap/src/uncompressed/TweenLite.js'),
	// 		"TweenMax": path.resolve('node_modules', 'gsap/src/uncompressed/TweenMax.js'),
	// 		"TimelineLite": path.resolve('node_modules', 'gsap/src/uncompressed/TimelineLite.js'),
	// 		"TimelineMax": path.resolve('node_modules', 'gsap/src/uncompressed/TimelineMax.js'),
	// 		"ScrollTo": path.resolve('node_modules', 'gsap/src/uncompressed/plugins/ScrollToPlugin.js'),
	// 		"ScrollMagic": path.resolve('node_modules', 'scrollmagic/scrollmagic/uncompressed/ScrollMagic.js'),
	// 		"animation.gsap": path.resolve('node_modules', 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js'),
	// 		"debug.addIndicators": path.resolve('node_modules', 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js'),
	// 	},
	// },

	module: {
		rules: [

			{
				test: /\.html$/,
				loader: 'html-loader',
				options: {
					minimize: true
				}
			},
			{
				test: /\.(scss)$/,
				use: [{
					loader: 'style-loader', // inject CSS to page
				}, {
					loader: 'css-loader', // translates CSS into CommonJS modules
				}, {
					loader: 'postcss-loader', // Run post css actions
					options: {
						plugins: function () { // post css plugins, can be exported to postcss.config.js
							return [
								require('precss'),
								require('autoprefixer')
							];
						}
					}
				}, {
					loader: 'sass-loader' // compiles Sass to CSS
				}]
			},
			{
				test: /\.(png|svg|jpg|gif)$/,
				use: [{
					loader: 'file-loader',
					options: {
						publicPath: 'assets/',
						outputPath: 'assets/'
					}
				}
				]
			},
			{
				enforce: "pre",
				test: /\.js$/,
				exclude: /node_modules/,
				loader: "eslint-loader",
			},
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['es2015']
					}
				}
			}
		]
	}
};
